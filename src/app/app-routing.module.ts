import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { CountyResolverService } from './shared/county-resolver.service';

const routes: Routes = [
  {
    path: 'country/:code', component: CountryDetailsComponent,
    resolve: {
      country: CountyResolverService
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
