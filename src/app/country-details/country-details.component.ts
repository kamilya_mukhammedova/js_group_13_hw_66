import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountryInfo } from '../shared/country-info.model';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent implements OnInit {
  country!: CountryInfo;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
   this.route.data.subscribe(info => {
     for(const key in <CountryInfo>info.country) {
       if (info.country[key] === '') {
         info.country[key] = '--------'
       } else if (info.country[key].length === 0) {
         info.country[key] = ['No borders countries']
       }
       if(key === 'flag') {
         const alpha3Code = info.country.alpha3Code.toLowerCase();
         info.country[key] = `http://146.185.154.90:8080/restcountries/data/${alpha3Code}.svg`;
       }
     }
     this.country = <CountryInfo>info.country;
   });
  }
}
