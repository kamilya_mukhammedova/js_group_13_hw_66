import { Component, OnDestroy, OnInit } from '@angular/core';
import { CountriesService } from '../shared/countries.service';
import { Country } from '../shared/country.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-countries-list',
  templateUrl: './countries-list.component.html',
  styleUrls: ['./countries-list.component.css']
})
export class CountriesListComponent implements OnInit, OnDestroy {
  countries!: Country[];
  countriesChangeSubscription!: Subscription;
  countriesFetchingSubscription!: Subscription;
  isLoading = false;

  constructor(private countriesService: CountriesService) { }

  ngOnInit() {
    this.countries = this.countriesService.getCountries();
    this.countriesChangeSubscription = this.countriesService.countriesChange.subscribe((countries: Country[]) => {
      this.countries = countries;
    });
    this.countriesFetchingSubscription = this.countriesService.countriesFetching.subscribe((isFetching: boolean) => {
      this.isLoading = isFetching;
    });
    this.countriesService.fetchCountries();
  }

  ngOnDestroy() {
    this.countriesChangeSubscription.unsubscribe();
    this.countriesFetchingSubscription.unsubscribe();
  }
}
