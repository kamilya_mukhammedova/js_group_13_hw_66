import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Country } from './country.model';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CountryInfo } from './country-info.model';

@Injectable()
export class CountriesService {
  countriesChange = new Subject<Country[]>();
  countriesFetching = new Subject<boolean>();

  private countriesArray: Country[] = [];

  constructor(private http: HttpClient) {}

  getCountries() {
    return this.countriesArray.slice();
  }

  fetchCountries() {
    this.countriesFetching.next(true);
    this.http.get<Country[]>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .pipe(map(result => {
        if(result === null) {
          return [];
        }
        return result.map(country => {
          return new Country(country.name, country.alpha3Code);
        });
      }))
      .subscribe(countries => {
        this.countriesArray = countries;
        this.countriesChange.next(this.countriesArray.slice());
        this.countriesFetching.next(false);
      }, () => {
        this.countriesFetching.next(false);
      });
  }

  fetchCountryInfo(code: string) {
    return this.http.get<CountryInfo>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${code}`)
      .pipe(map(result => {
        return new CountryInfo(
          result.flag,
          result.name,
          result.alpha3Code,
          result.nativeName,
          result.capital,
          result.region,
          result.borders,
          result.population
        );
      }));
  }
}
