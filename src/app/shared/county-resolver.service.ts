import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CountryInfo } from './country-info.model';
import { CountriesService } from './countries.service';

@Injectable({
  providedIn: 'root'
})
export class CountyResolverService implements Resolve<CountryInfo> {

  constructor(
    private route: ActivatedRoute,
    private countriesService: CountriesService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CountryInfo> {
    const countryCode = <string>route.params['code'];
    return this.countriesService.fetchCountryInfo(countryCode);
  }
}
