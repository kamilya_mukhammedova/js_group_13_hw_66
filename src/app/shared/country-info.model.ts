export class CountryInfo {
  constructor(
    public flag: string,
    public name: string,
    public alpha3Code: string,
    public nativeName: string,
    public capital: string,
    public region: string,
    public borders: string[],
    public population: number,
  ) {}
}
